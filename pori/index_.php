<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>UUSI PORI!</title>
<link href="../style.css" rel="stylesheet" type="text/css" />

<!-- Add jQuery library -->
<script type="text/javascript" src="../fancybox/lib/jquery-1.9.0.min.js"></script>

<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="../fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="../fancybox/source/jquery.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="../fancybox/source/jquery.fancybox.css" media="screen" />

<!-- Add Button helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="../fancybox/source/helpers/jquery.fancybox-buttons.css" />
<script type="text/javascript" src="../fancybox/source/helpers/jquery.fancybox-buttons.js"></script>

<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="../fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>

<!-- include Cycle plugin -->
<script type="text/javascript" src="http://cloud.github.com/downloads/malsup/cycle/jquery.cycle.all.latest.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.slideshow').cycle({
		fx:    'scrollDown', 
    	sync:  true, 
    	delay: -400 
	});
});
</script>

<script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();

			/*
			 *  Different effects
			 */

			// Change title type, overlay closing speed
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});

			// Disable opening and closing animations, change title type
			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});

			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

			// Remove padding, set opening and closing animations, close if clicked and disable overlay
			$(".fancybox-effects-d").fancybox({
				padding: 20,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});

			/*
			 *  Button helper. Disable animations, hide close button, change title type and content
			 */

			$('.fancybox-buttons')
			.attr('rel', 'media-gallery')
			.fancybox({
				padding	  : 0,
				closeBtn  : false,
				arrows    : false,
				nextClick : true,
				
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',


				helpers : {
					media : {},
					title : {
						type : 'outside'
					},
				},

				afterLoad : function() {
					this.title = (this.index + 1) + ' / ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});
			
			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});


		});
	</script>

</head>

<?php 
$f = fopen("../bg/upload/background.txt", 'r'); 
$line = fgets($f); 
fclose($f); 
$bgUrl = '../bg/upload/'.$line;
?>

<body style="background-image:url(<?php echo $bgUrl?>);">

<div id = "container"> 
<div id="linkit">
<div id="valikko" style="float:left; margin-left:0px;"><a href="../index.php">UUSI KAUPUNKI</a>!</div>
	<div id="valikko"><a href="../ajankohtaista.php">AJANKOHTAISTA</a></div>
	<div id="valikko"><a href="#">UUSI PORI!</a></div>
</div>

<div id="pohja">
	<div style="z-index:3; position: relative; width:320px; top: 0px; height: 300px; text-align:left; float:left;">
      <p style="font-size:70px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; line-height:70px; margin-top:10px; margin-bottom:0px;">UUSI PORI</p>
      <p style="margin-top: 50px; font-family:Georgia, 'Times New Roman', Times, serif; font-size:22px; line-height:32px; ">&nbsp;</p>
    </div>
    <div style="z-index:3; font-family:Georgia, 'Times New Roman', Times, serif; font-size:18px; line-height:28px; position: relative; width:600px; top: 0px; height: 300px; float:left;">
      <p style="margin-top:10px;">Osallistuva arkkitehtuurityöpaja järjestettiin Porissa kauppakeskus BePopissa 17.-18.4.2013.

Porin kaupunki haastoi Uusi Kaupunki-kollektiivin kehittämään uusia ajatuksia kuudesta eri ajakohtaisesta suunnittelukohteesta Porin keskusta-alueella. </p>
      <p style="margin-top:10px;">Haluaisitko kehittää työpajassa syntyneitä ajatuksia eteenpäin? Ole meihin mielellään yhteydessä: <a href="mailto:info@uusi-kaupunki.fi">info@uusi-kaupunki.fi</a>!</p>
    </div>
    
    <div style="margin-top:40px; margin-bottom:25px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:30px">
    <p>Kohteet</p></div>
    
    <div id="thumb">
      <p style="margin-top:0px;">Kaupungin aurinkoisin paikka!</p>    
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/01.jpg" title=""><img src="workshop/rudanko-kankkunen/thumb.jpg" width="280" height="187" border="0"/></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/02.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/03.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/04.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/05.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/06.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/07.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/08.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/09.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/10.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/11.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/12.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/13.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/14.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/15.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/16.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/17.jpg" title=""></a>            
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/18.jpg" title=""></a>
      <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/19.jpg" title=""></a>            
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/20.jpg" title=""></a>
      <p class="kuvateksti" ><a href="http://www.rudanko-kankkunen.com/">Arkkitehdit R+K</a></p> 
    </div>
    
    <div id="thumb">
      <p style="margin-top:0px;">Sesonkikaupunki</p>    
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/01.jpg" title=""><img src="workshop/jada/thumb.jpg" width="280" height="187" border="0"/></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/02.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/03.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/04.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/05.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/06.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/07.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/08.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/09.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/10.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/11.jpg" title=""></a>
      <p class="kuvateksti" ><a href="http://jada.eu">Jada</a></p> 
    </div>
    
    <div id="thumb" style="margin-right:0px;">
      <p style="margin-top:0px;">Sisäkaupunki</p>    
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/01.jpg" title=""><img src="workshop/futudesign/thumb.jpg" width="280" height="187" border="0"/></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/02.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/03.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/04.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/05.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/06.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/07.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/08.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/09.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/10.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/11.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/12.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/13.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/14.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/15.jpg" title=""></a>
      <p class="kuvateksti" ><a href="http://futudesign.com">Futudesign Oy</a></p> 
    </div>
        
    <div id="thumb">
      <p style="margin-top:0px;">Teollinen Pori</p>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/01.jpg" title=""><img src="workshop/pro-toto/thumb.jpg" width="280" height="187" border="0"/></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/02.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/03.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/04.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/05.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/06.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/07.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/08.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/09.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/10.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/11.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/12.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/13.jpg" title=""></a>
      <p class="kuvateksti" ><a href="http://www.pro-toto.eu/">Pro Toto</a></p> 
    </div>
            
    <div id="thumb">
      <p style="margin-top:0px;">Uusi piha!</p>    
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/01.jpg" title=""><img src="workshop/hukkatila/thumb.jpg" width="280" height="187" border="0"/></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/02.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/03.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/04.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/05.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/06.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/07.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/08.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/09.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/10.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/11.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/12.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/13.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/14.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/15.jpg" title=""></a>
      <p class="kuvateksti" ><a href="http://hukkatila.fi">Hukkatila Oy</a></p> 
    </div>
 
    <div id="thumb" style="margin-right:0px;">
      <p style="margin-top:0px;">Uusi tori!</p>    
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/01.jpg" title=""><img src="workshop/puisto/thumb.jpg" width="280" height="187" border="0"/></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/02.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/03.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/04.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/05.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/06.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/07.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/08.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/09.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/10.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/11.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/12.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/13.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/14.jpg" title=""></a>
      <p class="kuvateksti" ><a href="http://puisto-tyohuone.tumblr.com/">Studio Puisto</a></p> 
    </div>
    
    <div style="margin-top:40px; margin-bottom:25px;">
      <p style="margin-top:40px; margin-bottom:25px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:30px">Lisätietoja:</p>
    <p style="font-family:Georgia, 'Times New Roman', Times, serif; font-size:20px; line-height:30px; ">    Inari Virkkala<br>Arkkitehti SAFA (Hukkatila)
<br />
    +358 (0)40 574 1926<br />  <a href="mailto:info@uusi-kaupunki.fi">info@uusi-kaupunki.fi </a></p></div>    
  
  <div style="width:70px; height:70px; float:right; margin-top:-85px; text-align:right;">
  <a href="http://www.facebook.com/UusiKaupunki"><img src="../img/facebook-icon.jpg" width="50" height="50" border="0" /></a>
  </div>
    
</div>

<?php
	include '../adjektiivit.php';
?>

</div>
</body>
</html>
