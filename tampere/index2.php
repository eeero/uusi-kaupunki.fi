<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/UK_template.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>UUSI KAUPUNKI!</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<link href="../style.css" rel="stylesheet" type="text/css" />

<!-- Add jQuery library -->
<script type="text/javascript" src="http://www.uusi-kaupunki.fi/fancybox/lib/jquery-1.9.0.min.js"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="http://www.uusi-kaupunki.fi/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="http://www.uusi-kaupunki.fi/fancybox/source/jquery.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="http://www.uusi-kaupunki.fi/fancybox/source/jquery.fancybox.css" media="screen" />
<!-- Add Button helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="http://www.uusi-kaupunki.fi/fancybox/source/helpers/jquery.fancybox-buttons.css" />
<script type="text/javascript" src="http://www.uusi-kaupunki.fi/fancybox/source/helpers/jquery.fancybox-buttons.js"></script>
<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="http://www.uusi-kaupunki.fi/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="http://malsup.github.io/jquery.cycle.all.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.slideshow').cycle({
			fx:    'scrollDown', 
			sync:  true, 
			delay: -700 
		});
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		/*
		 *  Simple image gallery. Uses default settings
		 */

		$('.fancybox').fancybox();

		/*
		 *  Different effects
		 */

		// Change title type, overlay closing speed
		$(".fancybox-effects-a").fancybox({
			helpers: {
				title : {
					type : 'outside'
				},
				overlay : {
					speedOut : 0
				}
			}
		});

		// Disable opening and closing animations, change title type
		$(".fancybox-effects-b").fancybox({
			openEffect  : 'none',
			closeEffect	: 'none',

			helpers : {
				title : {
					type : 'over'
				}
			}
		});

		// Set custom style, close if clicked, change title type and overlay color
		$(".fancybox-effects-c").fancybox({
			wrapCSS    : 'fancybox-custom',
			closeClick : true,

			openEffect : 'none',

			helpers : {
				title : {
					type : 'inside'
				},
				overlay : {
					css : {
						'background' : 'rgba(238,238,238,0.85)'
					}
				}
			}
		});

		// Remove padding, set opening and closing animations, close if clicked and disable overlay
		$(".fancybox-effects-d").fancybox({
			padding: 20,

			openEffect : 'elastic',
			openSpeed  : 150,

			closeEffect : 'elastic',
			closeSpeed  : 150,

			closeClick : true,

			helpers : {
				overlay : null
			}
		});

		/*
		 *  Button helper. Disable animations, hide close button, change title type and content
		 */

		$('.fancybox-buttons')
		.attr('rel', 'media-gallery')
		.fancybox({
			padding	  : 0,
			closeBtn  : false,
			arrows    : false,
			nextClick : true,
			
			openEffect  : 'none',
			closeEffect : 'none',

			prevEffect : 'none',
			nextEffect : 'none',


			helpers : {
				media : {},
				title : {
					type : 'outside'
				},
			},

			afterLoad : function() {
				this.title = (this.index + 1) + ' / ' + this.group.length + (this.title ? ' - ' + this.title : '');
			}
		});
		
		$('.fancybox-media')
			.attr('rel', 'media-gallery')
			.fancybox({
				openEffect : 'none',
				closeEffect : 'none',
				prevEffect : 'none',
				nextEffect : 'none',

				arrows : false,
				helpers : {
					media : {},
					buttons : {}
				}
			});


	});
</script>

<?php 
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/bg/upload/background.txt";
   $f = fopen($path, 'r'); 
   $line = fgets($f); 
   fclose($f);
   $bgUrl = 'http://uusi-kaupunki.fi/bg/upload/'.$line;
?>

</head>

<body style="background-image:url(<?php echo $bgUrl?>);">

<div id = "container"> 
<div id="linkit">
	<a href="../index.php"><div id="valikko" style="float:left; margin-left:0px;">UUSI KAUPUNKI!</div></a>
	<a href="../blog.php"><div id="valikko">BLOG</div></a>
	<a href="../tyopajat.php"><div id="valikko">TYÖPAJAT</div></a>
    <a href="../info.php"><div id="valikko">MISTÄ ON KYSE?</div></a>
</div>

<div id="kehys">    
<!-- InstanceBeginEditable name="sisältö" -->
<div id="pohja" style="background:url(tampere_bg.jpg); background-position:center;">
      <div style="z-index:3; position: relative; width:920px; top: 0px; text-align:center; float:left;">
             <div style="width:920px;">
                 <p style="font-size:80px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; line-height:90px; margin-top:40px; margin-bottom:40px; color:#FFF">UUSI TAMPERE!<br />
               28.-29.8.2013</p>
             </div>     
      </div>
</div>
<div id="pohja_tyhja">
 <script async src="http://uusikaupunkimaps-production.herokuapp.com/maps/1/embed"></script> 
</div>
<!-- InstanceEndEditable -->
    
 
<div id="pohja">
	<div style="z-index:3; position: relative; width:500px; height:170px; top: 0px;  margin-bottom:0px; text-align:left; float:left;">
        <div style="width:280px; height:170px; margin-bottom:0px; ">
            <p style="margin-top:0px; margin-bottom:0px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:30px">Lisätietoja</p>
            <p style="font-family:Georgia, 'Times New Roman', Times, serif; font-size:18px; line-height:27px;  margin-bottom:0px; ">Inari Virkkala<br>Arkkitehti SAFA (Hukkatila)<br /> +358 (0)40 574 1926<br />
            <a href="mailto:info@uusi-kaupunki.fi">info@uusi-kaupunki.fi </a></p>
        </div>
	</div>
    
    <div style="z-index:3; font-family:Georgia, 'Times New Roman', Times, serif; font-size:18px; line-height:27px; position: relative; width:350px; top: 0px; height: 80px; float:left; vertical-align:bottom;">
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/fi_FI/all.js#xfbml=1";
          fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));</script>
        <div class="fb-like" data-send="true" data-href="http://uusi-kaupunki.fi" data-width="350" data-show-faces="false" style="vertical-align:bottom"></div>
    </div>
    
    <div style="width:50px; height:50px; float:right; margin-top:-5px; text-align:right; margin-bottom: 0px; margin-right: -2px; position:relative;">
        <a href="http://www.facebook.com/UusiKaupunki"><img src="../img/facebook-icon.jpg" width="50" height="50" border="0" /></a>
    </div>
    <div style="width:400px; height:30px; float:right; margin-top:45px; text-align:right; margin-bottom: 0px; position:relative; bottom:-15px; "></div>    
</div>

</div>

<?php 
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/adjektiivit.php";
   include_once($path);
?>


</div>

</body>
<!-- InstanceEnd --></html>
