<?php 
if(time() > $_COOKIE['vinkki']){
	setcookie('vinkki', time()+10000, time()+10000, '/', 'uusi-kaupunki.fi'); 
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/UK_template.dwt" codeOutsideHTMLIsLocked="false" -->
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>UUSI KAUPUNKI!</title>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-43540614-1', 'uusi-kaupunki.fi');
  ga('send', 'pageview');

</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<link href="../style.css" rel="stylesheet" type="text/css" />

<!-- Add jQuery library -->
<script type="text/javascript" src="http://www.uusi-kaupunki.fi/fancybox/lib/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="http://www.uusi-kaupunki.fi/fancybox/lib/jquery-1.10.1.min.js"></script>

<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="http://www.uusi-kaupunki.fi/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="http://www.uusi-kaupunki.fi/fancybox/source/jquery.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="http://www.uusi-kaupunki.fi/fancybox/source/jquery.fancybox.css" media="screen" />
<!-- Add Button helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="http://www.uusi-kaupunki.fi/fancybox/source/helpers/jquery.fancybox-buttons.css" />
<script type="text/javascript" src="http://www.uusi-kaupunki.fi/fancybox/source/helpers/jquery.fancybox-buttons.js"></script>
<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="http://www.uusi-kaupunki.fi/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="http://malsup.github.io/jquery.cycle.all.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.slideshow').cycle({
			fx:    'scrollDown', 
			sync:  true, 
			delay: -700 
		});
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		/*
		 *  Simple image gallery. Uses default settings
		 */

		$('.fancybox').fancybox();

		/*
		 *  Different effects
		 */

		// Change title type, overlay closing speed
		$(".fancybox-effects-a").fancybox({
			helpers: {
				title : {
					type : 'outside'
				},
				overlay : {
					speedOut : 0
				}
			}
		});

		// Disable opening and closing animations, change title type
		$(".fancybox-effects-b").fancybox({
			openEffect  : 'none',
			closeEffect	: 'none',

			helpers : {
				title : {
					type : 'over'
				}
			}
		});

		// Set custom style, close if clicked, change title type and overlay color
		$(".fancybox-effects-c").fancybox({
			wrapCSS    : 'fancybox-custom',
			closeClick : true,

			openEffect : 'none',

			helpers : {
				title : {
					type : 'inside'
				},
				overlay : {
					css : {
						'background' : 'rgba(238,238,238,0.85)'
					}
				}
			}
		});

		// Remove padding, set opening and closing animations, close if clicked and disable overlay
		$(".fancybox-effects-d").fancybox({
			padding: 20,

			openEffect : 'elastic',
			openSpeed  : 150,

			closeEffect : 'elastic',
			closeSpeed  : 150,

			closeClick : true,

			helpers : {
				overlay : null
			}
		});

		/*
		 *  Button helper. Disable animations, hide close button, change title type and content
		 */

		$('.fancybox-buttons')
		.attr('rel', 'media-gallery')
		.fancybox({
			padding	  : 0,
			closeBtn  : false,
			arrows    : false,
			nextClick : true,
			
			openEffect  : 'none',
			closeEffect : 'none',

			prevEffect : 'none',
			nextEffect : 'none',


			helpers : {
				media : {},
				title : {
					type : 'outside'
				},
			},

			afterLoad : function() {
				this.title = (this.index + 1) + ' / ' + this.group.length + (this.title ? ' - ' + this.title : '');
			}
		});
		
		$('.fancybox-media')
			.attr('rel', 'media-gallery')
			.fancybox({
				openEffect : 'none',
				closeEffect : 'none',
				prevEffect : 'none',
				nextEffect : 'none',

				arrows : false,
				helpers : {
					media : {},
					buttons : {}
				}
			});
		
		
		$("#hide_toggle").mouseover(
			function() { 
				 $("#bg_teksti").css('visibility','visible'); 
				 $("#hide_toggle").css( 'cursor', 'pointer' );
			 });
		$("#hide_toggle").mouseout(function() { $("#bg_teksti").css('visibility','hidden'); });

		var onko = false;
		$("#hide_toggle").click(
			function () {
			    if(!onko){
					$("#bg_teksti").html("Paluu...");
					$('div').not('#hide_toggle, #bg_teksti').stop(true,true).fadeToggle(1000);
					$('.slideshow').hide();
				}
				if(onko){
					$("#bg_teksti").html("Näytä taustakuva!");
					$('div').not('#hide_toggle, #bg_teksti').stop(true,true).fadeToggle(1000);
					$('.slideshow').hide();
					setTimeout(function() {$('.slideshow').fadeIn('2000');}, 4000);
				}
			onko = !onko;
			}
		);

	});
</script>




<?php 
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/bg/upload/background.txt";
   $f = fopen($path, 'r'); 
   $line = fgets($f); 
   fclose($f);
   $bgUrl = 'http://uusi-kaupunki.fi/bg/upload/'.$line;
?>

</head>

<body style="background-image:url(<?php echo $bgUrl?>);">

<div id="hide_toggle" style="padding: 15px; float:left; width:120px; height:30px; position:absolute; z-index: 100;">
<div id="bg_teksti" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; visibility:hidden;">Näytä taustakuva!</div>
</div>

<div id = "container"> 
<div id="linkit">
	<a href="../index.php"><div id="valikko" style="float:left; margin-left:0px;">UUSI KAUPUNKI!</div></a>
	<a href="../blog.php"><div id="valikko">BLOG</div></a>
	<a href="../tyopajat.php"><div id="valikko">TYÖPAJAT</div></a>
    <a href="../info.php"><div id="valikko">MISTÄ ON KYSE?</div></a>
</div>

<div id="kehys">    
<!-- InstanceBeginEditable name="sisältö" -->
<div id="pohja" style="background:url(tampere_bg.jpg); background-position:center;">
      <div style="z-index:3; position: relative; width:920px; top: 0px; text-align:center; float:left;">
             <div style="width:920px;">
                 <p style="font-size:80px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; line-height:90px; margin-top:40px; margin-bottom:40px; color:#FFF">UUSI TAMPERE!<br />
               28.-29.8.2013</p>
             </div>     
      </div>
</div>


<div id="pohja">
  
  <div style="z-index:3; position: relative; width:320px; top: 0px; text-align:left; float:left;">
    <div style="width:280px;">
      <p style="font-size:60px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; line-height:65px; margin-top:5px; margin-bottom:0px;">
           Työpaja</p>
          <p style="margin-top:20px; font-family:Georgia, 'Times New Roman', Times, serif; font-size:18px; line-height:27px; "><em>Uusi Tampere! -työpaja  <br />
  Liikekeskus Siperiassa 28.–29.8.2013</em></p>
</div>     
    </div>
  <div style="z-index:3; font-family:Georgia, 'Times New Roman', Times, serif; font-size:18px; line-height:27px; position: relative; width:600px; top: 0px; float:left; ">
        <p style="margin-top:10px;">Tampereen Arkkitehtuuriviikon yhteydessä järjestetyssä Uusi Tampere!<br />
          -suunnittelutyöpajassa kehitettiin uusia ajatuksia ja näkökulmia<br />
          Tampereen kehittämiseen. Tampereen kaupunki haastoi Uusi Kaupunki<br />
        -kollektiivin pohtimaan kuutta jaankohtaista suunnittelukohdetta:</p>
    <p style="margin-top:10px;">- Hakametsän jäähalli <em>(Pro Toto)</em><br />
      - Molinin tontti <em>(Futudesign)</em><br />
      - Pop-up -hotelli Linnan juhlille<em> (Studio Puisto)</em><br />
      - Pyynikintori ja taidemuseo<em> (Arkkitehdit R+K)</em><br />
      - Ratina <em>(Jada)</em><br />
      - Vapaa-ajan ympäristöt / “Tampereen Baana”<em> (Hukkatila)</em></p>
    <p style="margin-top:10px;">&nbsp;</p>
    <p style="margin-top:10px;"> </p>
    </div>
  
  <div id="thumb">
      <p style="margin-top:0px;">Hakametsän jäähalli</p>    
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/01.jpg" title=""><img src="workshop/pro-toto/thumb.jpg" width="280" height="187" border="0"/></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/02.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/03.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/04.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/05.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/06.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/07.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/08.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/09.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/10.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/11.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/12.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/13.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/14.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/15.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="pro-toto" href="workshop/pro-toto/16.jpg" title=""></a>
      <p class="kuvateksti" ><a href="http://www.pro-toto.eu/">Pro Toto</a></p> 
    </div>
    
    <div id="thumb">
      <p style="margin-top:0px;">Molinin tontti</p>    
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/01.jpg" title=""><img src="workshop/futudesign/thumb.jpg" width="280" height="187" border="0"/></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/02.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/03.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/04.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/05.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/06.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/07.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/08.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/09.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/10.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="futudesign" href="workshop/futudesign/11.jpg" title=""></a>
      <p class="kuvateksti" ><a href="http://futudesign.com">Futudesign</a></p> 
    </div>
    
    <div id="thumb" style="margin-right:0px;">
      <p style="margin-top:0px;">Pop-up -hotelli Linnan juhlille</p>    
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/01.jpg" title=""><img src="workshop/puisto/thumb.jpg" width="280" height="187" border="0"/></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/02.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/03.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/04.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/05.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/06.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/07.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/08.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/09.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/10.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/11.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/12.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/13.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/14.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/15.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="puisto" href="workshop/puisto/16.jpg" title=""></a>
      <p class="kuvateksti" ><a href="http://puisto-tyohuone.tumblr.com/">Studio Puisto</a></p>
    </div>
        
    <div id="thumb">
      <p style="margin-top:0px;"><span style="margin-top:10px;">Pyynikintori ja taidemuseo</span></p>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/01.jpg" title=""><img src="workshop/rudanko-kankkunen/thumb.jpg" width="280" height="187" border="0"/></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/02.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/03.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/04.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/05.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/06.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/07.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/08.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/09.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/10.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/11.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/12.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/13.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/14.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/15.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/16.jpg" title=""></a>
      <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/17.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/18.jpg" title=""></a>
      <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/19.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="rudanko-kankkunen" href="workshop/rudanko-kankkunen/20.jpg" title=""></a>
      <p class="kuvateksti" ><a href="http://www.rudanko-kankkunen.com/">Arkkitehdit R+K</a></p> 
    </div>
            
    <div id="thumb">
      <p style="margin-top:0px;"><span style="margin-top:10px;">Ratina</span></p>    
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/01.jpg" title=""><img src="workshop/jada/thumb.jpg" width="280" height="187" border="0"/></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/02.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/03.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/04.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/05.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/06.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/07.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/08.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/09.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/10.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/11.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/12.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/13.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/14.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="jada" href="workshop/jada/15.jpg" title=""></a>
      <p class="kuvateksti" ><a href="http://jada.eu">Jada</a></p>
    </div>
 
    <div id="thumb" style="margin-right:0px;">
      <p style="margin-top:0px;"><span style="margin-top:10px;">Treitti</span></p>    
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/01.jpg" title=""><img src="workshop/hukkatila/thumb.jpg" width="280" height="187" border="0"/></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/02.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/03.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/04.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/05.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/06.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/07.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/08.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/09.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/10.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/11.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/12.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/13.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/14.jpg" title=""></a>
      <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/15.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/16.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/17.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/18.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/19.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/20.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/21.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/22.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/23.jpg" title=""></a>
	  <a class="fancybox-buttons" data-fancybox-group="hukkatila" href="workshop/hukkatila/24.jpg" title=""></a>
      <p class="kuvateksti" ><a href="http://hukkatila.fi">Hukkatila</a></p> 
    </div>
    
  
  
</div>

<div id="pohja" style="margin-bottom:0px;">

     <div style="z-index:3; position: relative; width:320px; top: 0px; text-align:left; float:left;">
         <div style="width:280px;">
           <p style="font-size:60px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; line-height:65px; margin-top:5px; margin-bottom:0px;">
           Idea-<br />
           kartta</p>
         </div>     
     </div>
     <div style="z-index:3; font-family:Georgia, 'Times New Roman', Times, serif; font-size:18px; line-height:27px; position: relative; width:600px; top: 0px; float:left; ">
        <p style="margin-top:10px;">Työpajassa kokeiltiin Uusi Kaupungin kehittämää karttasovellusta asukkaiden omien ideoiden keräämiseen. Työpajan suunnittelukohteet on merkitty karttaan mustalla ja tamperelaisilta kerätyt ideat sinisellä. Eniten kommentteja saatiin kerättyä suunnittelukohteisiin liittyen. Muita kaupunkilaisia kiinnostavia kehittämiskohteita ovat esimerkiksi pyöräily-yhteyksien sujuvoittaminen kaupungin länsiosassa, erilaiset virkistys- ja vapaa-ajan kohteet sekä rantaväylän tunnelivaihtoehdot.</p>
        <p class="kuvateksti" style="text-align:right; float:right; font-family:Arial, Helvetica, sans-serif; margin-bottom:-10px; margin-top:-10px; height:0px;"><a href="ideakartta.html" target="_blank">Avaa isompi kartta!</a></p>

     </div>

</div>

<div id="pohja_tyhja" style="margin-top:0px;">
 <script id="ukm" type="text/javascript">$.getScript("http://uusikaupunkimaps-production.herokuapp.com/maps/1/embed?width=1000px&height=700px");</script>
</div>

<div id="pohja">

     <div style="z-index:3; position: relative; width:320px; top: 0px; text-align:left; float:left;">
         <div style="width:280px;">
           <p style="font-size:60px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; line-height:65px; margin-top:5px; margin-bottom:0px;">
           Aika-<br />
         taulu</p>
        </div>     
     </div>
     <div style="z-index:3; font-family:Georgia, 'Times New Roman', Times, serif; font-size:18px; line-height:27px; position: relative; width:600px; top: 0px; float:left; ">
              <p style="margin-top:10px;"><strong>Keskiviikko 28.8.2013</strong></p>
              <p style="margin-top:10px;">9- Uusi Kaupunki -kollektiivin työpaja liikekeskuksessa käynnistyy                </p>
              <p style="margin-top:10px;">10-14 Paikalliseksperttien haastatteluja työpajassa tai suunnittelukohteissa<br />
                </p>
              <p style="margin-top:10px;">12-18 Vuorovaikututteista työskentelyä yleisön kanssa Uusi Kaupunki-työpajassa Liikekeskus Siperiassa </p>
<p style="margin-top:10px;">20- Majoitus, ideasuunnitelmien hiominen </p>
              <p style="margin-top:10px;"><strong><br />
              Torstai 29.8.2013                </strong></p>
              <p style="margin-top:10px;">9-15 Vuorovaikutteista työskentelyä yleisön kanssa Uusi Kaupunki -työpajassa liikekeskuksessa </p>
              <p style="margin-top:10px;">15-16 Ideoiden esittely Liikekeskus Siperian aulassa                </p>
     </div>

</div>

<!-- InstanceEndEditable -->
    
 
<div id="pohja">
	<div style="z-index:3; position: relative; width:510px; height:170px; top: 0px;  margin-bottom:0px; text-align:left; float:left;">
        <div style="width:230px; height:170px; margin:0px 20px 0px 0px; float:left">
            <p style="margin-top:0px; margin-bottom:0px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:30px; line-height:30px;">Lisätietoja</p>
            <p style="font-family:Georgia, 'Times New Roman', Times, serif; font-size:18px; line-height:27px;  margin-bottom:0px; ">Hilla Rudanko<br>
              Arkkitehti SAFA<br />
              (Arkkitehdit R+K)<br /> 
            +358 40 7187671</p>        </div>
        <div style="width:230px; height:170px; margin:0px; float:left">
            <p style="margin-top:0px; margin-bottom:0px; font-family:Georgia, 'Times New Roman', Times, serif;  font-weight:normal; font-size:18px; line-height:30px;"><em>info@uusi-kaupunki.fi </em></p>
            <p style="font-family:Georgia, 'Times New Roman', Times, serif; font-size:18px; line-height:27px;  margin-bottom:0px; ">Jussi Vuori<br>
              Arkkitehti SAFA <br />
              (JADA)<br /> 
              +358 50 3447026</p>
        </div>        
	</div>
    
    <div style="z-index:3; font-family:Georgia, 'Times New Roman', Times, serif; font-size:18px; line-height:27px; position: relative; width:320px; top: 0px; height: 80px; float:left; vertical-align:bottom;">
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/fi_FI/all.js#xfbml=1";
          fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));</script>
        <div class="fb-like" data-send="true" data-href="http://uusi-kaupunki.fi" data-width="350" data-show-faces="false" style="vertical-align:bottom"></div>
    </div>
    
    <div style="width:50px; height:50px; float:right; margin-top:-5px; text-align:right; margin-bottom: 0px; margin-right: -2px; position:relative;">
        <a href="http://www.facebook.com/UusiKaupunki"><img src="../img/facebook-icon.jpg" width="50" height="50" border="0" /></a>
    </div>
</div>

</div>

<?php 
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/adjektiivit.php";
   include_once($path);
?>


</div>

</body>
<!-- InstanceEnd --></html>
