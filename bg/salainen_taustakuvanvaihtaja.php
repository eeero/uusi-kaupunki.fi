<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>

<?php
// IMAGE UPLOAD

$allowedExts = array("jpg", "jpeg", "gif", "png", "JPG", "PNG", "GIF");
$extension = end(explode(".", $_FILES["file"]["name"]));
if ((($_FILES["file"]["type"] == "image/gif")
|| ($_FILES["file"]["type"] == "image/jpeg")
|| ($_FILES["file"]["type"] == "image/png")
|| ($_FILES["file"]["type"] == "image/pjpeg"))
&& ($_FILES["file"]["size"] < 6000000)
&& in_array($extension, $allowedExts))
  {
  if ($_FILES["file"]["error"] > 0)
    {
    echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
    }
  else
    {
    
   if (file_exists("upload/" . $_FILES["file"]["name"]))
      {
      echo $_FILES["file"]["name"] . " already exists. ";
      }
    else
      {
	  $imgName = clean_image_name($_FILES["file"]["name"]);
	  $id = uniqid();
	  $imgName = $id . $imgName;
      move_uploaded_file($_FILES["file"]["tmp_name"], "upload/" . $imgName);
	  
	  // SAVE URL TXT AND REMOVE OLD
		$f = fopen("upload/background.txt", 'r'); 
		$oldfile = fgets($f); 
		fclose($f);
		//echo "vanha:";
		//echo "upload/".$oldfile;
		unlink("upload/".$oldfile);
		
		$f=fopen("upload/background.txt","w+");
		fwrite($f,$imgName);
		fclose($f);
		echo "uusi taustakuva:";		
		echo "upload/".$imgName;
		
	  // RESIZE
	  
	  include('SimpleImage.php');
      $image = new SimpleImage();
      $image->load("upload/" . $imgName);
	  $imgHeight = $image->getHeight();
	  if($imgHeight > 1400){
	      $image->resizeToHeight(1400);
	  }
	  $image->save("upload/" . $imgName);
	    
     }
   }
}
else{
  echo "Valitse uusi taustakuva:";
}

function clean_image_name($str){
	$str = preg_replace("/[^\w\.-]/", "", strtolower($str));
	return $str;
}

?>


</head>

<body <?php $f = fopen("upload/background.txt", 'r'); $line = fgets($f); fclose($f); echo 'background="upload/'.$line;?>">
<div id="imgUploader">
  <form action="salainen_taustakuvanvaihtaja.php" method="post"
	enctype="multipart/form-data">
	<input type="file" name="file" id="file" value="Valitse kuva">
	<input type="submit" name="submit" value="Vaihda tausta">
  </form>
</div>
</body>
</html>