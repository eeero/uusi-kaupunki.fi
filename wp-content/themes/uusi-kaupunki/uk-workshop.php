<?php
/*
Template Name: Workshop
*/
?>
<?php get_header(); ?>
<?php global $page_colour; ?>
<section id="content" role="main">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
		<header class="header container container-fluid wide">
			<div class="row">
				<h2 class="entry-title col-xs-12 pre-heading"><?php the_field('uk_top_heading'); ?></h2>
			</div>
		</header>
		
		<section class="entry-content">
		
			<div class="content-top container container-fluid narrow">
				<div class="row">
					<div class="content-top-image col-xs-12">
						<?php //Lasketaan kuvien määrä; jos on useita, käytetään slideria ?>
						<?php $main_images_count = count(get_field('uk_main_images')); ?>
						<ul class="<?php echo ($main_images_count > 1) ? 'bxslider' : ''; ?>">
							<?php if( have_rows('uk_main_images') ): ?>
								<?php while( have_rows('uk_main_images') ): the_row();
									$main_image = wp_get_attachment_image_src(get_sub_field('uk_main_image'), 'large-crop'); ?>	
									<li><img src="<?php echo $main_image[0]; ?>" alt="<?php echo get_the_title(get_sub_field('uk_main_image')) ?>"></li>
								<?php endwhile; ?>
							<?php endif; ?>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="content-top-heading col-sm-4">
						<h1><?php the_field('uk_heading'); ?></h1>
					</div>
					<div class="content-top-description col-sm-8">
						<?php the_field('uk_main_content'); ?>
					</div>
				</div>
			</div>
			
			<div class="content-main container container-fluid wide">
				<div class="row">
					<div class="mini-heading col-xs-12">
						<p><span></span></p>
					</div>
				</div>
				
				<div class="row">
						<?php if( have_rows('uk_presentations') ): //Haetaan esitykset ja tulostetaan ne ?>
						<?php $i = 0; ?>
						<?php while( have_rows('uk_presentations') ): the_row(); $i++;	
							$presentation_featured_image = wp_get_attachment_image_src(get_sub_field('uk_presentation_featured_image'), 'medium-crop'); ?>
							<div class="col-sm-4 third">
								<h3><?php the_sub_field('uk_presentation_title'); ?></h3>
								<?php if ( have_rows('uk_presentation_images') ): ?>
									<div class="slideshow third-img-container">
									<?php $j = 0; ?>
									<?php $presentation_image_array = array(); ?>
									<?php while (have_rows('uk_presentation_images') ): the_row(); $j++;
										$presentation_image = wp_get_attachment_image_src(get_sub_field('uk_presentation_image'), 'large');
										$presentation_image_array[] = $presentation_image[0];
										if ($j == 1) { ?>
											<a href="<?php echo $presentation_image[0]; ?>" rel="presentation-<?php echo $i; ?>"><img src="<?php echo $presentation_featured_image[0]; ?>"></a>
										<?php } else { ?>
											<a href="<?php echo $presentation_image[0]; ?>" rel="presentation-<?php echo $i; ?>" style="display: none;"></a>
										<?php } ?>
										
									<?php endwhile; ?>
									</div>
								<?php endif; ?>
								<div class="third-content">
									<p><?php the_sub_field('uk_presentation_creator'); ?></p>
									<?php //echo '<pre>' . print_r($presentation_image_array, 1) . '</pre>'; ?>
									<p><a href="javascript:void(0);" class="open-slideshow read-more" data-slideshow-id="<?php echo $i; ?>">Katso esitys</a></p>
								</div>
							</div><!-- .third -->
							<?php echo ($i % 3 == 0) ? '</div><div class="row">' : ''; ?>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
				
				<div class="row">
					<div class="mini-heading col-xs-12">
						<p><span></span></p>
					</div>
				</div>
			
			</div>
			
			<div class="content-additional container container-fluid narrow">
				<div class="row">
					<div class="col-xs-12">
						<p class="h2 pre-heading">Ideakartta</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 content-additional-map">
						<script id="ukm" type="text/javascript">
							$ = jQuery;
							$.getScript("http://uusikaupunkimaps-production.herokuapp.com/maps/<?php the_field('uk_ideakartta_id'); ?>/embed?height=655px&admin_icon=%2Fwp-content%2Fthemes%2Fuusi-kaupunki%2Fimages%2Fmarker-black.png&icon=%2Fwp-content%2Fthemes%2Fuusi-kaupunki%2Fimages%2Fmarker-<?php echo $page_colour; ?>.png");
						</script>
						<?php the_field('uk_ideakartta'); ?>
					</div>
				</div>
				
				<div class="row">
					<div class="col-sm-4 content-additional-heading">
						<?php if (get_field('uk_bottom_content')) { ?>
							<p class="h1"><?php the_field('uk_bottom_heading'); ?></p>
						<?php } ?>
					</div>
					<div class="col-sm-8 content-additional-description">
						<?php the_field('uk_bottom_content'); ?>
						<div class="content-top-share">
							<p><span class="underline">Jaa:</span>&nbsp;&nbsp;&nbsp;&nbsp;<a id="share-button-facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>" class="share-button" target="_blank">Facebook</a>&nbsp;&nbsp;&nbsp;&nbsp;<a id="share-button-twitter" href="http://twitter.com/share?text=<?php the_title(); ?>&url=<?php the_permalink(); ?>&hashtags=Uusi_Kaupunki" target="_blank" class="share-button">Twitter</a></p>
						</div>
					</div>
				</div>
			</div>
			
			<?php /*if ( has_post_thumbnail() ) { the_post_thumbnail(); }*/ ?>
			<?php //the_content(); ?>
			<div class="entry-links"><?php wp_link_pages(); ?></div>
			
		</section>
	</article>
	<?php //if ( ! post_password_required() ) comments_template( '', true ); ?>
	<?php endwhile; endif; ?>
</section>

<?php get_sidebar(); ?>
	
<?php get_footer(); ?>