<!DOCTYPE html>
<!--[if lt IE 7]>      <html id="top" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html id="top" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html id="top" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html id="top" class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( ' | ', true, 'right' ); ?></title>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
<?php wp_head(); ?>
</head>
<?php global $page_colour; ?>
<?php global $first_load; ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-43540614-1', 'uusi-kaupunki.fi');
  ga('send', 'pageview');

</script>
<body <?php body_class($page_colour); ?>>
<?php if ((get_the_id() == 5) && ($first_load === true)) : ?>
	<div id="loadingWrapper" style="position: fixed; background: <?php echo uk_get_page_colour_code($page_colour); ?>; height: 100%; width: 100%; top: 0; z-index: 10000;"></div>
<?php endif ?>
<div id="wrapperWrapper">
	<div id="wrapper" class="hfeed">
		<nav id="menu" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
		</nav>
		<header id="header" role="banner" class="container container-fluid wide">
			<div class="row">
				<section id="branding" class="col-xs-12">
					<div id="site-logo">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
							<img src="<?php bloginfo('template_directory'); ?>/images/uusi-kaupunki-logo-2x.png" alt="<?php bloginfo( 'description' ); ?>" title="<?php bloginfo( 'description' ); ?>" width="335" height="84">
						</a>
					</div>
				</section>
			</div>
		</header>
	<!--<div id="container" class="container wide">-->