<?php
/*
Template Name: TEST
*/
?>
<?php get_header(); ?>
<section id="content" role="main">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
		<section class="entry-content">
		
			<div class="content-top container container-fluid narrow">
				<div class="row">
					<div class="content-top-image col-xs-12">
						<?php //!Pääkuva(t) ?>
						<?php if( have_rows('uk_main_images') ): ?>
							<?php $main_images_count = count(get_field('uk_main_images')); ?>
							<ul class="<?php echo ($main_images_count > 1) ? 'bxslider slider-list' : 'slider-list'; ?>">
								<?php //Tulostetaan kuvat mahdolliseen slideriin ?>
								<?php while( have_rows('uk_main_images') ): the_row();
									//Otetaan talteen mahdollinen kuvalle/tekstille määritelty linkki
									$main_image_link = '';
									$main_image_link .= get_sub_field('uk_main_image_link');
									//Tarkistetaan, onko kyseessä kuva. Jos on, tulostetaan se.
									if (get_sub_field('uk_main_image')) {
										$main_image = wp_get_attachment_image_src(get_sub_field('uk_main_image'), 'large-crop'); ?>
										<li class="slider-image">
											<?php if (!empty($main_image_link)) { ?>
												<a href="<?php echo $main_image_link; ?>">
											<?php } ?>
													<img src="<?php echo $main_image[0]; ?>" alt="<?php echo get_the_title(get_sub_field('uk_main_image')) ?>">
											<?php if (!empty($main_image_link)) { ?>
												</a>
											<?php } ?>
										</li>
									<?php
									} else if (get_sub_field('uk_main_image_text')) {
										//Jos ei ole kuvaa, tulostetaan teksti ?>
										<li class="slider-text">
											<?php
											echo (!empty($main_image_link) ? '<a href="' . $main_image_link . '" class="slider-text-link">' : '<div class="slider-text-link">');
												the_sub_field('uk_main_image_text');
											echo (!empty($main_image_link) ? '</a>' : '</div>'); ?>
										</li>
									<?php } ?>
								<?php endwhile; ?>
							</ul>
						<?php endif; ?>
					</div>
				</div>
			</div>
			
			<div class="content-additional container container-fluid narrow">
			</div>
			
			<?php /*if ( has_post_thumbnail() ) { the_post_thumbnail(); }*/ ?>
			<div class="entry-links"><?php wp_link_pages(); ?></div>
			
		</section>
	</article>
	<?php // if ( ! post_password_required() ) comments_template( '', true ); ?>
	<?php endwhile; endif; ?>
</section>

<?php get_sidebar(); ?>
	
<?php get_footer(); ?>