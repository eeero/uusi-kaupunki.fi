<?php
add_action( 'after_setup_theme', 'uusikaupunki_setup' );
function uusikaupunki_setup()
{
load_theme_textdomain( 'uusikaupunki', get_template_directory() . '/languages' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
global $content_width;
if ( ! isset( $content_width ) ) $content_width = 640;
register_nav_menus(
array( 'main-menu' => __( 'Main Menu', 'uusikaupunki' ) )
);
}
add_action( 'wp_enqueue_scripts', 'uusikaupunki_load_scripts' );
function uusikaupunki_load_scripts()
{
wp_enqueue_script( 'jquery' );
}
add_action( 'comment_form_before', 'uusikaupunki_enqueue_comment_reply_script' );
function uusikaupunki_enqueue_comment_reply_script()
{
if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}
add_filter( 'the_title', 'uusikaupunki_title' );
function uusikaupunki_title( $title ) {
if ( $title == '' ) {
return '&rarr;';
} else {
return $title;
}
}
add_filter( 'wp_title', 'uusikaupunki_filter_wp_title' );
function uusikaupunki_filter_wp_title( $title )
{
return $title . esc_attr( get_bloginfo( 'name' ) );
}
add_action( 'widgets_init', 'uusikaupunki_widgets_init' );
function uusikaupunki_widgets_init()
{
register_sidebar( array (
'name' => __( 'Sidebar Widget Area', 'uusikaupunki' ),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => "</li>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}
function uusikaupunki_custom_pings( $comment )
{
$GLOBALS['comment'] = $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php 
}
add_filter( 'get_comments_number', 'uusikaupunki_comments_number' );
function uusikaupunki_comments_number( $count )
{
if ( !is_admin() ) {
global $id;
$comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
return count( $comments_by_type['comment'] );
} else {
return $count;
}
}

// !UUSI KAUPUNKI
//---------------

//! Ladataan tarvittavat CSS-tiedostot
function uk_enqueue_styles() {
    wp_enqueue_style( 'plantin-std', get_template_directory_uri() . '/css/MyFontsWebfontsKit.css', false );
    wp_enqueue_style( 'bxslider-css', get_template_directory_uri() . '/scripts/jquery.bxslider.css', false );
    wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/scripts/jquery.fancybox.css', false );
}

//! Ladataan tarvittavat JS-tiedostot
function uk_enqueue_scripts() {
	wp_enqueue_script( 'bxslider', get_template_directory_uri() . '/scripts/jquery.bxslider.min.js', false );
	wp_enqueue_script( 'fancybox', get_template_directory_uri() . '/scripts/jquery.fancybox.pack.js', false );
	wp_enqueue_script( 'masonry', get_template_directory_uri() . '/scripts/masonry.pkgd.min.js', false );
	wp_enqueue_script( 'imagesloaded', get_template_directory_uri() . '/scripts/imagesloaded.pkgd.min.js', false );
	wp_enqueue_script( 'touchswipe', get_template_directory_uri() . '/scripts/jquery.touchSwipe.min.js', false );
	//wp_enqueue_script( 'uk_scripts', get_template_directory_uri() . '/scripts.js', false );
}

add_action( 'wp_enqueue_scripts', 'uk_enqueue_styles' );
add_action( 'wp_enqueue_scripts', 'uk_enqueue_scripts' );

//! Admin-puolen omat skriptit
function uk_admin_scripts() {
	wp_enqueue_script( 'uk_custom_admin_js', get_template_directory_uri() . '/scripts/admin.js', false );
}
add_action( 'admin_enqueue_scripts', 'uk_admin_scripts' );


//! Muuttuja, joka kertoo onko kyseessä session ensimmäinen sivulataus
//	Sen perusteella arvotaan session aikana käytettävä väri ja määritellään, näytetäänkö etusivulla "väriliukuma"
global $first_load;
$first_load = false;

if (!isset($_SESSION['colour'])) {
	$_SESSION['colour'] = uk_get_page_colour();
	$first_load = true;
}

function uk_get_page_colour() {
	$colour_nr = mt_rand(1, 3);
	if ($colour_nr == 1) {
		return 'blue';
	} else if ($colour_nr == 2) {
		return 'orange';
	} else if ($colour_nr == 3) {
		return 'green';
	} else {
		return 'black'; //Should not happen
	}
}

global $page_colour;
global $page_colour_code;
$page_colour = $_SESSION['colour'];
$page_colour_code = uk_get_page_colour_code($page_colour);

function uk_get_page_colour_code($colour) {
	if ($colour == 'blue') {
		return '#00abff';
	} else if ($colour == 'orange') {
		return '#ff7700';
	} else if ($colour == 'green') {
		return '#00b877';
	} else {
		return '#000'; //Should not happen
	}
}

//Simppeli funktio linkin luontia varten.
function uk_get_link( $url, $text ) {
	$returnvalue = '';
	if (strlen($url) > 0) {
		$returnvalue .= '<a href="' . $url . '">' . $text . '</a>';
	} else {
		$returnvalue .= $text;
	}
	return $returnvalue;
}

// Edit the read more link text
add_filter('get_the_content_more_link', 'custom_read_more_link');
add_filter('the_content_more_link', 'custom_read_more_link');

function custom_read_more_link() {
	return '<p><a class="more-link read-more" href="' . get_permalink() . '#more" rel="nofollow">Lue&nbsp;lisää</a></p>';
}

/*
function uk_list_child_pages() { 

	global $post;

	if ( is_page() && $post->post_parent ) {
		$childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0' );
	} else {
		$childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );	
	}

	if ( $childpages ) {
		$string = '<ul>' . $childpages . '</ul>';
	}

	return $string;

}
*/

//add_shortcode('wpb_childpages', 'wpb_list_child_pages');