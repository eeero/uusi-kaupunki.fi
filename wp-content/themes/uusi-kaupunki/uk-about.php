<?php
/*
Template Name: About
*/
?>
<?php get_header(); ?>
<section id="content" role="main">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
		<!--<header class="header container container-fluid wide">
			<div class="row">
				<h2 class="entry-title col-xs-12 pre-heading"><?php the_title(); ?></h2> <?php edit_post_link(); ?>
			</div>
		</header>-->
		
		<section class="entry-content">
		
			<div class="content-top container container-fluid narrow">
				<div class="row">
					<div class="content-top-image col-xs-12">
						<?php if( have_rows('uk_main_images') ): ?>
							<?php $main_images_count = count(get_field('uk_main_images')); ?>
							<ul class="<?php echo ($main_images_count > 1) ? 'bxslider' : ''; ?>">
								<?php while( have_rows('uk_main_images') ): the_row();
									$main_image = wp_get_attachment_image_src(get_sub_field('uk_main_image'), 'large-crop');
									$main_image_link = '';
									$main_image_link .= get_sub_field('uk_main_image_link'); ?>
									<li>
										<?php if (!empty($main_image_link)) { ?>
											<a href="<?php echo $main_image_link; ?>">
										<?php } ?>
												<img src="<?php echo $main_image[0]; ?>" alt="<?php echo get_the_title(get_sub_field('uk_main_image')) ?>">
										<?php if (!empty($main_image_link)) { ?>
											</a>
										<?php } ?>
									</li>
								<?php endwhile; ?>
							</ul>
						<?php endif; ?>
					</div>
				</div>
				
				<div class="row">
					<div class="content-top-heading col-sm-4">
						<h1><?php the_field('uk_heading'); ?></h1>
					</div>
					<div class="content-top-description col-sm-8">
						<div>
							<?php the_field('uk_main_content'); ?>
						</div>
						<?php if (get_field('uk_main_content_more')) { ?>
							<p><a href="#" data-toggle="toggle" data-target="uk_main_content_more" class="read-more"><?php echo (get_field('uk_main_content_more_link_text') ? get_field('uk_main_content_more_link_text') : 'Lue lisää'); ?></a></p>
							<div style="display: none;" id="uk_main_content_more">
								<?php the_field('uk_main_content_more'); ?>
							</div>
						<?php } ?>
					</div>
				</div>
			</div><!-- .content-top.container-narrow -->
			
			<div class="content-main container container-fluid wide">
			
				<div class="row">
					<div class="mini-heading col-xs-12">
						<p><span><?php the_field('uk_companies_heading'); ?></span></p>
					</div>
				</div>
				
				<div class="row">
					<?php
					//! YRITYSTEN LISTAUS
					if( have_rows('uk_companies') ): //Haetaan UK-yritykset ja tulostetaan ne ?>
						<?php $i = 0; ?>
						<?php while( have_rows('uk_companies') ): the_row(); $i++; ?>
							<div class="col-sm-4 third">
								<h3><a href="<?php the_sub_field('uk_company_url'); ?>" target="_blank"><?php the_sub_field('uk_company_name'); ?></a></h3>
								<div class="third-img-container">
									<?php $company_image = wp_get_attachment_image_src(get_sub_field('uk_company_image'), 'medium-crop'); ?>
									<a href="<?php the_sub_field('uk_company_url'); ?>" target="_blank"><img src="<?php echo $company_image[0]; ?>" alt="<?php echo get_the_title(get_sub_field('uk_company_image')) ?>" /></a>
								</div>
								<div class="third-content">
									<?php if (strlen(get_sub_field('uk_company_description')) > 10) {
										the_sub_field('uk_company_description'); //Yrityksen kuvausteksti
									} ?>
									<p><a href="<?php the_sub_field('uk_company_url'); ?>" target="_blank" class="read-more"><?php the_sub_field('uk_company_link_text'); ?></a></p>
								</div>
							</div><!-- .third -->
							<?php echo ($i % 3 == 0) ? '</div><div class="row">' : ''; ?>
						<?php endwhile; ?>
					<?php endif; ?>

				</div><!-- .row -->
			
			</div><!-- .container.wide -->
			
			<div class="content-additional container container-fluid narrow">
				<div class="row">
					<div class="col-xs-12">
						<p class="slogan">
							<?php //the_field('uk_bottom_slogan'); ?>
							<img src="<?php bloginfo('template_directory'); ?>/images/uusi-kaupunki-slogan-2x.png" alt="<?php bloginfo( 'description' ); ?>" title="<?php bloginfo( 'description' ); ?>" width="527" height="90">
						</p>
					</div>
				</div>
			</div>
			
			<?php /*if ( has_post_thumbnail() ) { the_post_thumbnail(); }*/ ?>
			<div class="entry-links"><?php wp_link_pages(); ?></div>
			
		</section>
	</article>
	<?php // if ( ! post_password_required() ) comments_template( '', true ); ?>
	<?php endwhile; endif; ?>
</section>

<?php get_sidebar(); ?>
	
<?php get_footer(); ?>