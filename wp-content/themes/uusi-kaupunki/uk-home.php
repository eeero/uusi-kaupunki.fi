<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>
<section id="content" role="main">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
		<section class="entry-content">
		
			<div class="content-top container container-fluid narrow">
				<div class="row">
					<div class="content-top-image col-xs-12">
						<?php //!Pääkuva(t) ?>
						<?php if( have_rows('uk_main_images') ): ?>
							<?php $main_images_count = count(get_field('uk_main_images')); ?>
							<ul class="<?php echo ($main_images_count > 1) ? 'bxslider slider-list' : 'slider-list'; ?>">
								<?php //Tulostetaan kuvat mahdolliseen slideriin ?>
								<?php while( have_rows('uk_main_images') ): the_row();
									//Otetaan talteen mahdollinen kuvalle/tekstille määritelty linkki
									$main_image_link = '';
									$main_image_link .= get_sub_field('uk_main_image_link');
									//Tarkistetaan, onko kyseessä kuva. Jos on, tulostetaan se.
									if (get_sub_field('uk_main_image')) {
										$main_image = wp_get_attachment_image_src(get_sub_field('uk_main_image'), 'large-crop'); ?>
										<li class="slider-image">
											<?php if (!empty($main_image_link)) { ?>
												<a href="<?php echo $main_image_link; ?>">
											<?php } ?>
													<img src="<?php echo $main_image[0]; ?>" alt="<?php echo get_the_title(get_sub_field('uk_main_image')) ?>">
											<?php if (!empty($main_image_link)) { ?>
												</a>
											<?php } ?>
										</li>
									<?php
									} else if (get_sub_field('uk_main_image_text')) {
										//Jos ei ole kuvaa, tulostetaan teksti ?>
										<li class="slider-text">
											<?php
											echo (!empty($main_image_link) ? '<a href="' . $main_image_link . '" class="slider-text-link">' : '<div class="slider-text-link">');
												the_sub_field('uk_main_image_text');
											echo (!empty($main_image_link) ? '</a>' : '</div>'); ?>
										</li>
									<?php } ?>
								<?php endwhile; ?>
							</ul>
						<?php endif; ?>
					</div>
				</div>
			</div>
			
			<div class="content-main container container-fluid wide">
				
				<div class="row">
					<div class="col-sm-4 third">
						<!--<p class="mini-heading"><span></span></p>-->
						<?php //Tulostetaan etusivun asetuksissa määritelty seuraava työpaja linkkeineen
						if ( get_field('uk_next_workshop_url') ) {
							$next_workshop_url = get_field('uk_next_workshop_url');
						} ?>
						<h3><?php
							echo (!empty($next_workshop_url) ? '<a href="' . $next_workshop_url . '">' : '');
							the_field('uk_next_workshop_heading');
							echo (!empty($next_workshop_url) ? '</a>' : ''); ?></h3>
						<?php $next_workshop_image = wp_get_attachment_image_src(get_field('uk_next_workshop_image'), 'medium-crop'); ?>
						<div class="third-img-container">
							<?php echo (!empty($next_workshop_url) ? '<a href="' . $next_workshop_url . '">' : ''); ?>
							<img src="<?php echo $next_workshop_image[0]; ?>">
							<?php echo (!empty($next_workshop_url) ? '</a>' : ''); ?>
						</div>
						<div class="third-content">
							<?php the_field('uk_next_workshop_text'); ?>
							<?php echo (!empty($next_workshop_url) ? '<p><a href="' . $next_workshop_url . '" class="read-more">Lue lisää</a></p>' : ''); ?>
						</div>
					</div>
					<div class="col-sm-4 third">
						<!--<p class="mini-heading"><span></span></p>-->
						<?php
						//Haetaan uusin uutinen
						$args = array (
									'posts_per_page' => '1',
									'orderby' => 'date',
									'post_status' => 'publish' );
						$recent_posts_query = new WP_Query( $args );
						//!Tulostetaan uusin uutinen
						while ( $recent_posts_query->have_posts() ) : $recent_posts_query->the_post(); ?>
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							<?php
								//Jos uutisella on 'featured image', käytetään sitä
								if (has_post_thumbnail(get_the_id())) { ?>
									<div class="third-img-container">
										<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('medium-crop'); ?></a>
									</div>
								<?php //Muuten käytetään uutisen 1. kuvaa
								} else if (have_rows('uk_main_images', get_the_id()) ) {
										while (have_rows('uk_main_images', get_the_id()) ) : the_row(); $i++;
											if (get_sub_field('uk_main_image')) { ?>
												<div class="third-img-container">
													<?php $news_main_image = wp_get_attachment_image_src(get_sub_field('uk_main_image', get_the_id()), 'medium'); ?>
													<a href="<?php the_permalink(); ?>"><img src="<?php echo $news_main_image[0]; ?>"></a>
												</div>
												<?php break; ?>
											<?php }
										endwhile;
								} ?>
							<div class="third-content">
								<?php
								//Jos uutiselle on määritelty esikatseluteksti, tulostetaan se
								if (get_field('uk_preview_text')) {
									the_field('uk_preview_text', get_the_id());
								//Muuten näytetään koko uutisteksti
								} else { ?>
									<div class="news-item-content">
									<?php the_content(); ?>
									</div>
								<?php } ?>
									
								<p><a href="http://uusi-kaupunki.fi/uutiset/" class="read-more">Lue lisää uutisia</a></p>
							</div>
						<?php endwhile;
						wp_reset_postdata();
						?>
					</div>
					<div class="col-sm-4 third">
						<!--<p class="mini-heading"><span></span></p>-->
						<?php //!Tulostetaan etusivun muokkaussivulla määritelty seuraava työpaja linkkeineen
						if ( get_field('uk_recent_project_url') ) {
							$recent_project_url = get_field('uk_recent_project_url');
						} ?>
						<h3><?php
							echo (!empty($recent_project_url) ? '<a href="' . $recent_project_url . '">' : '');
							the_field('uk_recent_project_heading');
							echo (!empty($recent_project_url) ? '</a>' : ''); ?></h3>
						<?php $recent_project_image = wp_get_attachment_image_src(get_field('uk_recent_project_image'), 'medium-crop'); ?>
						<div class="third-img-container">
							<?php echo (!empty($recent_project_url) ? '<a href="' . $recent_project_url . '">' : ''); ?>
							<img src="<?php echo $recent_project_image[0]; ?>">
							<?php echo (!empty($recent_project_url) ? '</a>' : ''); ?>
						</div>
						<div class="third-content">
							<?php the_field('uk_recent_project_text'); ?>
							<?php $workshop_url = get_field('uk_recent_project_url'); ?>
							<?php echo (!empty($recent_project_url) ? '<p><a href="' . $recent_project_url . '" class="read-more">Lue lisää</a></p>' : ''); ?>
						</div>
					</div>
				</div>
			
			</div>
			
			<div class="content-additional container container-fluid narrow">
			</div>
			
			<?php /*if ( has_post_thumbnail() ) { the_post_thumbnail(); }*/ ?>
			<div class="entry-links"><?php wp_link_pages(); ?></div>
			
		</section>
	</article>
	<?php // if ( ! post_password_required() ) comments_template( '', true ); ?>
	<?php endwhile; endif; ?>
</section>

<?php get_sidebar(); ?>
	
<?php get_footer(); ?>