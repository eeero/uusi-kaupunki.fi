<?php
//=========================================
//! VALITTU UUTINEN (yksittäinen artikkeli)
//=========================================

//Jos yksittäinen uutinen, tulostetaan päivämäärä, kuva(t), otsikko ja uutisteksti
if ( is_singular() ) { ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<section class="entry-content">
			<div class="content-top container-fluid container narrow">
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-8">
						<p class="mini-heading"><span><?php echo the_date(); ?></span></p>
					</div>
				</div><!-- .row -->
				<div class="row">
					<div class="content-top-image col-xs-12">
						<?php
						//Lasketaan, kuinka monta kuvaa on
						$main_image_count = count(get_field('uk_main_images'));
						//Jos kuvia on enemmän kuin yksi, otetaan käyttöön slider
						echo ($main_image_count > 1) ? '<ul class="bxslider">' : '';
							//Jos kuv(i)a on:
							if( have_rows('uk_main_images') ):
								while( have_rows('uk_main_images') ): the_row();
									$main_image = wp_get_attachment_image_src(get_sub_field('uk_main_image'), 'xlarge');
									echo ($main_image_count > 1) ? '<li>' : '';
										echo '<img src="' . $main_image[0] . '" alt="' . get_the_title(get_sub_field('uk_main_image')) . '">';
									echo ($main_image_count > 1) ? '</li>' : '';
								endwhile;
							endif;
						echo ($main_image_count > 1) ? '</ul>' : '';
						?>
					</div>
				</div><!-- .row -->
				<div class="row">
					<div class="content-top-heading col-sm-4">
						<h1 id="more"><?php the_title(); //!Uutisen otsikko ?></h1>
					</div>
					<div class="content-top-description single-news-item-content col-sm-8">
						<?php
						the_content(); //! Uutisen sisältö
						?>
						<div class="content-top-share">
							<?php //! Jakolinkit ?>
							<p><span class="underline">Jaa:</span>&nbsp;&nbsp;&nbsp;&nbsp;<a id="share-button-facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>" class="share-button" target="_blank">Facebook</a>&nbsp;&nbsp;&nbsp;&nbsp;<a id="share-button-twitter" href="http://twitter.com/share?text=<?php the_title(); ?>&url=<?php the_permalink(); ?>&hashtags=UusiKaupunki" target="_blank" class="share-button">Twitter</a></p>
						</div>
					</div>
				</div><!-- .row -->
			</div>
		</section>
		
		<?php
		//! MUUT UUTISET
		?>
		<section class="container container-fluid wide news-list">
			<?php $post_id = get_the_ID(); ?>
			<?php
			//Haetaan uusimmat uutiset
			$args = array (
						'posts_per_page' => '31',
						'orderby' => 'date',
						'post_status' => 'publish' );
			$recent_posts_query = new WP_Query( $args );
			$current_post_nr = $recent_posts_query->current_post + 1;
			
			//Tulostetaan uusimmat uutiset
			$j = 0;
			while ( $recent_posts_query->have_posts() ) : $recent_posts_query->the_post();
			
				global $more;
				$more = 0;

				if ($post_id != get_the_id()) {
					$j++;
					if ($j % 3 == 1) {
						echo '<div class="row">';
					} ?>
						<div class="col-sm-4 third news-item entry-content news-<?php echo $current_post_nr; ?>">
							<p><span class="underline"><?php the_time('j.n.Y'); ?></span></p>
							<h2 class="h3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
							<?php
								//Jos uutisella on 'featured image', käytetään sitä
								if (has_post_thumbnail(get_the_id())) { ?>
									<div class="third-img-container">
										<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('medium-crop'); ?></a>
									</div>
								<?php //Muuten käytetään uutisen 1. kuvaa
								} else if (have_rows('uk_main_images', get_the_id()) ) {
										while (have_rows('uk_main_images', get_the_id()) ) : the_row(); $i++;
											if (get_sub_field('uk_main_image')) { ?>
												<div class="third-img-container">
													<?php $news_main_image = wp_get_attachment_image_src(get_sub_field('uk_main_image', get_the_id()), 'medium'); ?>
													<a href="<?php the_permalink(); ?>"><img src="<?php echo $news_main_image[0]; ?>"></a>
												</div>
												<?php break; ?>
											<?php }
										endwhile;
								} ?>
							<div class="">
								<div class="news-item-content">
									<?php the_content('Lue lisää', FALSE); ?>
								</div>									
							</div>
						</div><!-- .news-item -->
					<?php if ($j % 3 == 0) {
						echo '</div>';
					}
				}
				if ($j >= 30) {
					break;
				}
			endwhile;
			wp_reset_postdata();
			?>
			
			<div class="row">
				<div class="col-sm-12">
					<p><a href="/uutisarkisto" class="news-archive-link">Uutisarkisto</a></p>
				</div>
			</div>

		</section>
	</article>
		<?php
		//! Jos uutislistaus, ei tulosteta mitään
		} ?>
		<?php /*<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a> */ ?>
		<?php //edit_post_link(); ?>
		<?php //if ( !is_search() ) get_template_part( 'entry', 'meta' ); ?>
	</header>
	<?php if ( !is_singular() ) {
		get_template_part( 'entry', ( is_archive() || is_search() ? 'summary' : 'content' ) );
	} ?>
	<?php //if ( !is_search() ) get_template_part( 'entry-footer' ); ?>
