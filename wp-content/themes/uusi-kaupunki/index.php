<?php
//=========
//! 	UUTISET-SIVU
//		Suurin osa tulostuksesta tapahtuu tiedostossa entry-content.php
//=========	
?>
<?php get_header(); ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<?php if ( !is_singular() ) {
				if ($wp_query->current_post == 0) { ?>
					<section id="content" role="main" class="container container-fluid wide news-list">
			<?php }
			} ?>
		<?php get_template_part( 'entry' ); ?>
		<?php //comments_template(); ?>
		<?php if ( !is_singular() ) {
				if (($wp_query->current_post + 1) == $wp_query->post_count) { ?>
					<div class="row">
						<div class="col-sm-12">
							<p><a href="/uutisarkisto" class="news-archive-link">Uutisarkisto</a></p>
						</div>
					</div>
				</section><!-- #content -->
					
			<?php }
			} ?>
	<?php endwhile; endif; ?>
	<?php //get_template_part( 'nav', 'below' ); ?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>