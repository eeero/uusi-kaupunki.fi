<?php
/*
Template Name: Archive
*/
?>
<?php get_header(); ?>
<section id="content" role="main">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
		<!--<header class="header container container-fluid wide">
			<div class="row">
				<h2 class="entry-title col-xs-12 pre-heading"><?php the_title(); ?></h2> <?php edit_post_link(); ?>
			</div>
		</header>-->
		
		<section class="entry-content news-archive">
		
			<div class="content-top container container-fluid wide">
				
				<?php
				// Näytetään kaikki uutisotsikot päivämäärineen
				query_posts( 'posts_per_page=-1' );
				$i = 0;
				while ( have_posts() ) : the_post();
					$i++;
					//if ($i % 3 == 1) {
						echo '<div class="row">';
					//} ?>
						<div class="col-sm-4 col-sm-offset-4 third news-item archive-item">
							<p class="date"><span class="underline"><?php the_time('j.n.Y'); ?></span></p>
							<h3 class="h3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						</div>
					<?php
					//if ($i % 3 == 0) {
						echo '</div>';
					//}
				endwhile; ?>
					
			</div><!-- .content-top.container-narrow -->
			
		</section>
	</article>
</section>

<?php get_sidebar(); ?>
	
<?php get_footer(); ?>