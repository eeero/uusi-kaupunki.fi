<?php
//=================================================================================
//! 	Täällä tulostetaan suurin osa Uutiset-sivun sisällöstä (ks. myös index.php)
//=================================================================================
?>
<?php global $page_colour; ?>
<?php $current_post_nr = $wp_query->current_post + 1; ?>

<?php //! Joka kolmas 1. uutisesta alkaen: uusi rivi
if ($current_post_nr % 3 == 1) { ?>
	<div class="row">
<?php } ?>
		<div class="entry-content col-sm-4 third news-item news-<?php echo $current_post_nr; ?>">
			<p><span class="underline"><?php the_time('j.n.Y'); ?></span></p>
			<h2 class="h3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<?php //if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
			<?php //Haetaan uutisen kuvat ?>
			<?php if ( have_rows('uk_main_images') ): ?>
				<?php $news_images_count = count(get_field('uk_main_images', get_the_id())); ?>
				<div class="slideshow third-img-container<?php echo ($news_images_count > 1) ? ' multi-image' : ''; ?>">
				<?php $j = 0; ?>
				<?php //$news_image_array = array(); ?>
				<?php while (have_rows('uk_main_images') ): the_row(); $j++;
					//
					$news_image = wp_get_attachment_image_src(get_sub_field('uk_main_image'), 'xlarge');
					$news_image_medium = wp_get_attachment_image_src(get_sub_field('uk_main_image'), 'medium');
					//$news_image_array[] = $news_image[0];
					if ($j == 1) { ?>
						<a href="<?php echo $news_image[0]; ?>" rel="news-<?php the_id(); ?>" class="news-image-link">
							<?php if ($news_images_count > 1) { ?>
								<img src="<?php bloginfo('template_directory'); ?>/images/plus-<?php echo $page_colour; ?>.png" class="news-plus" title="Katso kaikki kuvat">
							<?php } ?>
							<img src="<?php echo $news_image_medium[0]; ?>">
						</a>
					<?php } else { ?>
						<a href="<?php echo $news_image[0]; ?>" rel="news-<?php the_id(); ?>" style="display: none;"></a>
					<?php } ?>
					
				<?php endwhile; ?>
				</div>
			<?php endif; ?>
			<div class="news-item-content">
			<?php the_content('Lue lisää'); ?>
			</div>
			<?php // <div class="entry-links"><?php wp_link_pages(); </div>?>
		</div>
		
<?php //Joka kolmas 3. uutisesta alkaen:
if (( $current_post_nr % 3 == 0 ) || ( $current_post_nr == $wp_query->post_count )) { ?>
	</div><!-- .row -->
<?php } ?>