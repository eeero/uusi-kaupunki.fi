<?php
/*
Template Name: Workshops
*/
?>
<?php get_header(); ?>
<section id="content" role="main">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
		<section class="entry-content">
			
			<div class="content-main container container-fluid wide" id="workshops-list">
				<div class="row">
				<?php
				$subpages = get_pages( array(
					'child_of' => $post->ID,
					'sort_column' => 'menu_order', 
					'sort_order' => 'desc',
					'post_status' => 'publish' ) );
				$i = 0;
				//Haetaan Workshops-sivun alasivut eli yksittäiset työpajat
				foreach( $subpages as $subpage ) {
					$i++;
					$content = $subpage->post_content;
					//if ( ! $content ) {// Check for empty page
					//	continue;
					//}
					$content = apply_filters( 'the_content', $content ); ?>
					<div class="col-sm-6 half">
						<?php
							$j = 0;
							//Jos työpajalla on kuvia:
							if( have_rows('uk_main_images', $subpage->ID) ): ?>
								<?php //$main_images_count = count(get_field('uk_main_images', $subpage->ID)); ?>
								<ul>
								<?php
									//Haetaan työpajan kuvat
									while ( have_rows('uk_main_images', $subpage->ID ) ) : the_row(); $j++;
										//Haetaan kuva
										$workshop_image = wp_get_attachment_image_src(get_sub_field('uk_main_image', $subpage->ID), 'medium-crop');
										//Tulostetaan 1. kuva
										if ($j == 1) { ?>
										<a href="<?php echo get_page_link( $subpage->ID ); ?>" class="workshop-text-link">
											<li class="half-img-container">
												<h2 style="color: #000;"><?php the_field('uk_top_heading', $subpage->ID); ?></h2>
												<img src="<?php echo $workshop_image[0]; ?>" alt="<?php echo get_the_title(get_sub_field('uk_main_image')) ?>">
											</li>
										</a>
										<?php } ?>
									<?php endwhile; ?>
								</ul>
							<?php endif; ?>
						<div class="half-content">
							<?php
							//Tapahtuman mahdollinen kuvaus
							if (strlen($content) > 10) {
								echo $content;
							} ?>
							<p><a href="<?php echo get_page_link( $subpage->ID ); ?>" class="read-more">Lue lisää</a></p>
						</div>
					</div><!-- .half -->
					<?php echo ($i % 2 == 0) ? '</div><div class="row">' : ''; ?>
				<?php }	?>
				</div> <!-- .row -->
							
			</div>
			
			<div class="content-additional container container-fluid narrow">
			</div>
			
			<?php /*if ( has_post_thumbnail() ) { the_post_thumbnail(); }*/ ?>
			<div class="entry-links"><?php wp_link_pages(); ?></div>
			
		</section>
	</article>
	<?php // if ( ! post_password_required() ) comments_template( '', true ); ?>
	<?php endwhile; endif; ?>
</section>

<?php get_sidebar(); ?>
	
<?php get_footer(); ?>