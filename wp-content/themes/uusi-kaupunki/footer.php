<?php global $page_colour_code; ?>
<?php global $page_colour; ?>
<?php global $first_load; ?>
		<footer id="footer" role="contentinfo">
			<div class="container container-fluid wide">
				<div class="row">
					<div class="mini-heading col-xs-12">
						<p><span>Lisätietoja</span></p>
					</div>
				</div><!-- .row -->
				<div class="roww copyright" id="yhteystiedot">
					<div class="footer-1">
						<p>Inari Virkkala<br>
						Arkkitehti SAFA<br>
						(Hukkatila Oy)<br>
						<a href="tel:+358405741926">+358&nbsp;40&nbsp;5741926</a></p>
					</div>
					<div class="footer-2">
						<p>Jussi Vuori<br>
						Arkkitehti SAFA<br>
						(JADA)<br>
						<a href="tel:+358503447026">+358&nbsp;50&nbsp;3447026</a></p>
					</div>
					<div class="footer-3">
						<p><strong><a href="mailto:info@uusi-kaupunki.fi">info@uusi-kaupunki.fi</a></strong></p>
					</div>
					<div class="footer-6">
						<a href="https://www.facebook.com/UusiKaupunki" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/facebook-<?php echo $page_colour; ?>.png" alt="Uusi Kaupunki Facebookissa" title="Uusi Kaupunki Facebookissa"></a>
					</div>
					<div class="footer-5">
						<?php if (function_exists('wp_email_capture_form')) { wp_email_capture_form(); } ?>
						<!--Uutiskirje: <input type="text" placeholder="sähköpostiosoitteesi">-->
					</div>
					<div class="footer-4">
						&copy; <?php echo date('Y'); ?> Uusi Kaupunki
					</div>

				</div><!-- .row -->
				<div class="row visible-xs back-to-top">
					<div class="col-xs-12">
						<a href="#top" class="scrollTo">Takaisin ylös</a>
					</div>
				</div>
			</div><!-- .container -->
		</footer>
		
	</div><!-- #wrapper -->
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/scripts.js"></script>
</div><!-- #wrapperWrapper -->
<script>

(function($) {

	<?php
	//Jos etusivu ja session 1. lataus, työnnetään värillinen overlay syrjään pienen viiveen jälkeen
	if ((get_the_id() == 5) && ($first_load === true)) { ?>

		setTimeout(function() {
			$('#loadingWrapper').animate({
				top: $(this).height()
			}, 300, function() {
				$(this).remove();
			});
		}, 1500);
		
	<?php }	?>

	$('.third .bxslider, .half .bxslider').bxSlider({
		auto: true,
		easing: 'linear',
		pause: 5000,
		pager: false,
		mode: 'horizontal',
		autoHover: true
	});

	//Asetetaan slider-elementtien korkeus valmiiksi, jotta tekstilaatikot ovat kuvien korkuisia
	var sliderElement = $('.page-template-uk-home-php .content-top-image .slider-list');
	var sliderHeight = sliderElement.outerWidth() / 1.618;
	sliderElement.find('.slider-text').css('height', sliderHeight);
	
	$('.content-top-image .bxslider').bxSlider({
		auto: true,
		easing: 'linear',
		pause: 5000,
		pager: false,
		mode: 'horizontal',
		autoHover: true,
		preloadImages: 'all'
	});
	
	$('.slideshow a').fancybox({
		openEffect : 'none',
		closeEffect : 'none',
		nextEffect : 'none',
		prevEffect : 'none',
		helpers : {
			overlay : {
				css : {
					'background' : '<?php echo $page_colour_code; ?>'
				}
			},
			title: {
            	type: 'outside'
			}
		},
		padding: 10,
		loop: false,
		afterShow: function() {
			$('.fancybox-wrap').swipe({
				swipe : function(event, direction) {
					if (direction === 'right') {
						$.fancybox.prev( direction );
					}
					else if (direction === 'left') {
						$.fancybox.next( direction );
					}
				},
				allowPageScroll: 'vertical'
			});
        },
		afterLoad : function() {
			this.title = (this.index + 1) + '/' + this.group.length + (this.title ? ' - ' + this.title : '');
			console.log(this);
		},
		fitToView : false,
		maxWidth : '100%'
	});
	
	$('a.open-slideshow').click(function(e) {
		var el, id = $(this).data('slideshowId');
		if (id) {
			el = $('.slideshow a[rel=presentation-' + id + ']:eq(0)');
			e.preventDefault();
			el.click();
		}
	});
	
	$('#wp-email-capture-email-widget').prop('placeholder', 'sähköpostiosoitteesi');
	
	//Yhteystietolinkille skrollaus
	$('#menu-item-310 a').addClass('scrollTo');
	$('#menu-item-310 a').on('click', function() {
		setTimeout(function() {
			$('#wrapper').addClass('circled');
	  	}, 1000);
	});
	
	//Skrollaa ankkurilinkissä määritellyn id:n kohdalle
	function scrollToAnchor(linkId) {
		var aTag = $(linkId);
		$('html, body').animate({ scrollTop: aTag.offset().top }, 'slow');
	}

	//Lisää ankkurilinkeille luokka scrollTo
	$('a.scrollTo').on('click', function() {
		scrollToAnchor($(this).prop('hash'));
	});
	
	$('.news-item-content img').wrap('<div class="news-item-content-container"></div>');
	$('.news-item-content a img, .single-news-item-content a img').closest('a').prop('target', '_blank');
	
	//"Lue lisää" -ominaisuus
	$('[data-toggle="toggle"]').on('click', function(e) {
		var $this = $(this);
		e.preventDefault();
		var toggle_el = $this.data('target');
		$('#' + toggle_el).toggle(200);
		$this.remove();
	});
	
	<?php //!~IP-rajoitus
if ($_SERVER['REMOTE_ADDR'] == '91.153.123.60') { ?>
/*

	var workshopImageContainer = $('#workshops-list .ws-container');
	var workshopImageHeight = (workshopImageContainer.first().closest('.half').width()) / 1.5;
	
	$('.ws-container').css('height', workshopImageHeight);
*/
	
<?php } ?>
		
	

})(jQuery);
</script>
<?php wp_footer(); ?>
</body>
<?php if (get_the_id() == 5) { ?>
<script type="text/javascript">
/*$('#wrapperWrapper').css('opacity', 0);
$(window).load(function() {
	$('#loadingWrapper').css('opacity', 1);
});*/
</script>
<?php } ?>
</html>