<?php ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>UUSI KAUPUNKI BLOG</title>
<link href="style.css" rel="stylesheet" type="text/css" />


<!-- Add jQuery library -->
<script type="text/javascript" src="fancybox/lib/jquery-1.9.0.min.js"></script>

<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="fancybox/source/jquery.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="fancybox/source/jquery.fancybox.css" media="screen" />

<!-- Add Button helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="fancybox/source/helpers/jquery.fancybox-buttons.css" />
<script type="text/javascript" src="fancybox/source/helpers/jquery.fancybox-buttons.js"></script>

<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>

<!-- include Cycle plugin -->
<script type="text/javascript" src="http://cloud.github.com/downloads/malsup/cycle/jquery.cycle.all.latest.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.slideshow').cycle({
		fx:    'scrollDown', 
    	sync:  true, 
    	delay: -400 
	});
});
</script>

<script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();

			/*
			 *  Different effects
			 */

			// Change title type, overlay closing speed
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});

			// Disable opening and closing animations, change title type
			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});

			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

			// Remove padding, set opening and closing animations, close if clicked and disable overlay
			$(".fancybox-effects-d").fancybox({
				padding: 20,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});

			/*
			 *  Button helper. Disable animations, hide close button, change title type and content
			 */

			$('.fancybox-buttons')
			.attr('rel', 'media-gallery')
			.fancybox({
				padding	  : 0,
				closeBtn  : false,
				arrows    : false,
				nextClick : true,
				
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',


				helpers : {
					media : {},
					title : {
						type : 'outside'
					},
				},

				afterLoad : function() {
					this.title = (this.index + 1) + ' / ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});
			
			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});


		});
	</script>

</head>

<?php 
$f = fopen("bg/upload/background.txt", 'r'); 
$line = fgets($f); 
fclose($f); 
$bgUrl = 'bg/upload/'.$line;
?>

<body style="background-image:url(<?php echo $bgUrl?>);">

<div id = "container"> 
<div id="linkit">
<div id="valikko" style="float:left; margin-left:0px;"><a href="index.php">UUSI KAUPUNKI</a>!</div>
	<div id="valikko"><a href="ajankohtaista.php">AJANKOHTAISTA</a></div>
	<div id="valikko"><a href="pori/index.php">UUSI PORI!</a></div>
</div>

<div id="pohja">
<div style="margin-top:10px; margin-bottom:25px; font-family:Arial, Helvetica, sans-serif; width:920px;">
    <p style="font-weight:bold; font-size:50px; margin:0px 0px 30px 0px; ">BLOG</p>
    
	<?php 
    /* Short and sweet */
    define('WP_USE_THEMES', false);
    require('blog/wp-blog-header.php');
    ?>

 <?php query_posts('posts_per_page=20'); ?>
 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
 <div style="margin:0px 0px 50px 0px; height:150px">
	 <div style="width:230px; float:left; margin:15px 0px 0px 0px;">
 	<p style="font-size:26px; font-weight:bold; margin:0px 0px 15px 0px;"><?php the_title(); ?>
 	<p style="font-size:12px; font-weight:normal; margin:0px 0px 0px 0px; line-height: 20px;">
	<?php the_time('F jS, Y') ?> by <?php the_author_posts_link() ?><br />
 	Posted in <?php the_category(', '); ?></p>
	</div>

	<div class="postmetadata" style="font-family:Georgia, 'Times New Roman', Times, serif; font-size:17px; line-height:24px; font-weight:normal; margin:0px 0px 30px 30px; float:left; width:660px">
	<?php the_content(); ?></div>

 </div>

 <?php endwhile; else: ?>

 <?php endif; ?>


</div>
<div style="margin:100px 0px 50px 0px; font-family:Georgia, 'Times New Roman', Times, serif; font-size:16px; line-height:23px; float:right; text-align:left; width:100%;">
    <a href="http://uusi-kaupunki.fi/blog">Arkisto</a>
  
  <div style="width:50px; height:50px; float:right; margin-top:-45px; text-align:right; margin-bottom: 0px; position:relative; bottom:-15px;">
  <a href="http://www.facebook.com/UusiKaupunki"><img src="img/facebook-icon.jpg" width="50" height="50" border="0" /></a>
  </div>
</div>   

</div>

<?php
	include 'adjektiivit.php';
?>

</div>
</body>
</html>
